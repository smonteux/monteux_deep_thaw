mkdir Bsnowfence_rarefied1000_tsv
for file in ./Bsnowfencerarefied1000/*
do
	file_name="$(basename "$file" | cut -d "." -f 1)"
	biom convert -i "$file" -o Bsnowfence_rarefied1000_tsv/"$file_name" --table-type "OTU table" --to-tsv
done
