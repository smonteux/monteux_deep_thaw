# README #

**Long-term *in situ* permafrost thaw effects on bacterial communities and potential aerobic respiration** 

Sylvain Monteux, James T. Weedon, Gesche Blume-Werry, Konstantin Gavazov, Vincent E.J. Jassey, Margareta Johansson, Frida Keuper, Carolina Olid, Ellen Dorrepaal

*The ISME Journal* **2018**, [https://doi.org/10.1038/s41396-018-0176-z](https://doi.org/10.1038/s41396-018-0176-z)

Data and figure-production script (R/ folder) are also found on figshare at [https://doi.org/10.6084/m9.figshare.5977471](https://doi.org/10.6084/m9.figshare.5977471)

### Structure ###

* Monteux_Deepthaw_bioinfo.ipynb is the core bioinformatics pipeline file (Jupyter notebook)
* R/ stores the data files and script (Monteux_Deepthaw_figs_and_tabs.R) that can be used as they are to reproduce the published figures
* raw/ stores the Sequence Read Archive accession numbers list (SRA_Acc_List.txt), and will receive the raw .fastq files
* Map_Bsnowfence.txt is the QIIME mapping file
* scripts/ store various scripts and databases used in the bioinformatics pipeline
	* 97_otu_taxonomy.txt and 97_otus.fasta are GreenGenes 13.8 database files (McDonald et al., 2012, ISME J)
	* clean_tsv_from_biom_rarefied_tables.sh batch-runs cleaner.awk on Bsnowfence_rarefied1000_tsv/ into Bsnowfence_rarefied1000_tsv/clean/
	* cleaner.awk automates the clean-up of tsv files created from biom files for easier import in R, removing all "#", the first line ("Constructed from biom file") and modifying the second line ("OTU Id" becomes "OTU_Id")
	* control_OTU_removal.R identifies and removes from the initial OTU table the OTUs with relative abundance > 5% in the control sample
	* convert_table.sh batch converts the 100 rarefactions from biom to tsv into Bsnowfence_rarefied1000_tsv/
	* die.py, fasta_number.py, progress.py, uc.py, uc2otutab2.py are from Robert Edgar's UPARSE supporting Python scripts (https://drive5.com/python/python_scripts.tar.gz)
	* gold.fa is the GOLD databse (Haas et al., 2011, Genome Research)
	* make_consensus_from_rarefactions.R computes an average abundance table from the 100 tables in Bsnowfence_rarefied1000_tsv/clean/
	* relabel.awk is custom-made by J.T.Weedon and changes the Illumina headers of a fastq file into a simpler format (>S\*sequence_number\*; barcode=\*index_number\*)
	* remove_primers.awk is custom-made by J.T.Weedon and removes the primers from merged reads (22 first bp and 16 last bp)
* Rnew/ will be created as the final step of the Jupyter notebook, and will have a similar structure to the R/ folder. The results should match those obtained using the R/ folder save for possible stochastic differences (e.g. in rarefactions)
* OTU_DESEQ.fasta includes the representative sequences of the abundant OTUs found in Supplementary Table S2 and seen in Fig 1C and Supplementary Figure S5

### Contact ###

* Corresponding author: Sylvain Monteux [sylvain.monteux@slu.se](mailto:sylvain.monteux@slu.se)
* [On ResearchGate](https://www.researchgate.net/profile/Sylvain_Monteux)